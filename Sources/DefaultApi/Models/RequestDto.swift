//
//  RequestDto.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation

public struct RequestDto: Codable {

    let baseUrl: String
    let path: String
    let params: [String: String]?
    let body: [String: String]?
    let headers: [String: String]?
    let method: HttpMethodType

    public init(baseUrl: String,
                path: String,
                params: [String: String]? = nil,
                body: [String: String]? = nil,
                headers: [String: String]? = nil,
                method: HttpMethodType) {
        self.baseUrl = baseUrl
        self.path = path
        self.params = params
        self.body = body
        self.headers = headers
        self.method = method
    }
}
