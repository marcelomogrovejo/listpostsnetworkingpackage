//
//  HttpMethodType.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation

public enum HttpMethodType: String, Codable {

    case get = "GET"
    case post = "POST"

}
