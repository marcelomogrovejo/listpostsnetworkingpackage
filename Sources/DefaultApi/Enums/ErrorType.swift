//
//  ErrorType.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation

public protocol CustomErrorConvertible {

    associatedtype CustomErrorType

    func toCustomError() -> CustomErrorType
}

public enum NetworkError: LocalizedError {

    // MARK: - Generic networking errors
    case serverConnectionError

    case invalidUrlRequested
    case invalidUrlRequestedResponse

    case unknownHttpResponse
    case noDataError
    case badRequest
    case unauthorized
    case forbidden(message: String)
    case notFound(message: String)
    case requestTimeOut
    case conflict(message: String)
    case badGateway
    case serviceUnavailable
    case gatewayTimeOut
    case internalServerError

    case customErrorWith(message: String)

    /// Description
    public var errorDescription: String? {
        switch self {
        case .customErrorWith(let message),
                .forbidden(let message),
                .notFound(let message),
                .conflict(let message):
            return message
        default:
            return ""
        }
    }
}
