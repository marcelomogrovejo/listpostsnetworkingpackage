//
//  ApiAlamofireRepository.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger

public class ApiAlamofireRepository: RepositoryProtocol {

    public init() {
        #if DEBUG
            NetworkActivityLogger.shared.level = .debug
            NetworkActivityLogger.shared.startLogging()
        #endif
    }

    // MARK: - NetworkingRepository delegate

    // TODO: RequestDtoProtocol ??
    
    public func fetch(request: RequestDto, completion: @escaping (Result<Data, NetworkError>) -> Void) {

        let urlString = request.baseUrl + request.path
        guard let url = URL(string: urlString) else {
            preconditionFailure("Invalid URL used to create URL instance.")
        }

        AF.request(url,
                   method: toAlamofireHttpMethod(request.method),
                   parameters: request.params,
                   encoding: URLEncoding.default,
                   headers: toAlamofireHeaders(request.headers)
        ).responseData { response in
            switch response.result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                
                // TODO: convert AFError to NetworkError
                
                print("XXX Alamofire error: \(error.localizedDescription)")
                
                break
            }
        }
    }

    // MARK - Private methods

    private func toAlamofireHttpMethod(_ method: HttpMethodType) -> HTTPMethod {
        switch method {
        case .get: return HTTPMethod.get
        case .post: return HTTPMethod.post
        }
    }

    private func toAlamofireHeaders(_ headers: [String: String]?) -> HTTPHeaders {
        var httpHeaders: HTTPHeaders = [:]
        guard let headers = headers else {
            return httpHeaders
        }

        for header in headers {
            let httpHeader = HTTPHeader(name: header.key, value: header.value)
            httpHeaders.add(httpHeader)
        }
        return httpHeaders
    }
}
