//
//  RepositoryProtocol.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation

public protocol RepositoryProtocol {

    func fetch(request: RequestDto, completion: @escaping (Result<Data, NetworkError>) -> Void)

}

