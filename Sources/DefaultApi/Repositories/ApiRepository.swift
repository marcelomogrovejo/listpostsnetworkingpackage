//
//  ApiRepository.swift
//  ListPostsNetworkingPackage
//
//  Created by Marcelo Mogrovejo on 10/02/2023.
//

import Foundation

public class ApiRepository: RepositoryProtocol {

    let urlSession: URLSession?

    public init(urlSession: URLSession? = URLSession.shared) {
        self.urlSession = urlSession
    }

    public func fetch(request: RequestDto, completion: @escaping (Result<Data, NetworkError>) -> Void) {

        let urlString = request.baseUrl + request.path
        guard let url = URL(string: urlString) else {
            preconditionFailure("Invalid URL used to create URL instance.")
        }
        let urlRequest = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.httpMethod = request.method.rawValue

        let httpHeaders = request.headers
        urlRequest.allHTTPHeaderFields = httpHeaders

        let session = urlSession?.dataTask(with: urlRequest as URLRequest) { data, response, error in

            if let error = error {
                let errorCode: Int = (error as NSError).code
                switch errorCode {
                case -1004, -1001:
                    completion(.failure(.serverConnectionError))
                    return
                default:
                    completion(.failure(.invalidUrlRequestedResponse))
                    return
                }
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.unknownHttpResponse))
                return
            }

            switch httpResponse.statusCode {
            case 200, 201:
                if let data = data {
                    completion(.success(data))
                } else {
                    completion(.failure(.noDataError))
                }

                /*
                 TODO: Handle specific endpoint errors when they are defined.
                 Also 300-399 error codes.
                 */


            case 400:
                completion(.failure(.badRequest))
            case 401:
                completion(.failure(.unauthorized))
            case 403:
                if let data = data {
                    completion(.success(data))
                }
            case 404:
                if let data = data {
                    completion(.success(data))
                }
            case 408:
                completion(.failure(.requestTimeOut))
            case 409:
                if let data = data {
                    completion(.success(data))
                }
            case 502:
                completion(.failure(.badGateway))
            case 503:
                completion(.failure(.serviceUnavailable))
            case 504:
                completion(.failure(.gatewayTimeOut))
            default:
                debugPrint("@@@ httpResponse.statusCode: \(httpResponse.statusCode)")
                completion(.failure(.internalServerError))
            }
        }
        session?.resume()
    }

}
