// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "ListPostsNetworkingPackage",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "ListPostsNetworkingPackage",
            targets: ["DefaultApi"]),
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.5.0")),
        .package(url: "https://github.com/konkab/AlamofireNetworkActivityLogger.git", .upToNextMajor(from: "3.4.0"))
    ],
    targets: [
        .target(
            name: "DefaultApi",
            dependencies: ["Alamofire",
                          "AlamofireNetworkActivityLogger"
                          ]
        ),
        .testTarget(
            name: "DefaultApiTests",
            dependencies: ["DefaultApi"]),
    ]
)
