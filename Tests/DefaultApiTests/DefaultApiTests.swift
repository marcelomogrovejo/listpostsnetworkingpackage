import XCTest

@testable import DefaultApi

final class DefaultApiTests: XCTestCase {
    
    /*
    var sut: ApiRep?
    var stub: MGApiServiceStubs?
    
    override func setUp() {
        sut = MGApiService()
        stub = MGApiServiceStubs()
    }
    
    override func tearDown() {
        sut = nil
        stub = nil
    }
    
    func testMGApiService_GivenValidUsernameAndPassword_SuccessAuthenticationResults() {
        // Arrange
        stub?.enableAutenticateStubs()
//        stubs.authentitationResult = .success

        let validUsername = "test@gmail.com"
        let validPassword = "Test12389"
        let expectation = expectation(description: "Authentication success expectation")
        
        // Act
        
        sut?.authenticate(username: validUsername, password: validPassword) { result in
            switch result {
            case .success(let token):
                print("Authentication success: \(token)")
                
                // Assert
                XCTAssertNotNil(token, "Autentication token should be not nil but is was nil.")
                expectation.fulfill()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                
                XCTAssertNil(error, "error should be nil but it was \(error.localizedDescription)")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 4.0)
    }
    
    func testMGApiService_GivenValidUsernameAndPassword_FailureAuthenticationServiceErrorResults() {
        // Arrange
        stub?.enableAutenticateStubs()
        stub?.authentitationResult = .serviceError

        let validUsername = "test@gmail.com"
        let validPassword = "Test12389"
        let expectation = expectation(description: "Authentication service error failure expectation")
        
        // Act
        
        sut?.authenticate(username: validUsername, password: validPassword) { result in
            switch result {
            case .success(let token):
                print("Authentication success: \(token)")
                
                // Assert
                XCTAssertNotNil(token, "Autentication token should be not nil but is was nil.")
                expectation.fulfill()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
                
                XCTAssertNil(error, "error should be nil but it was \(error.localizedDescription)")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 4.0)
    }
    
    
    func testMGApiService_GivenValidToken_SuccessUnauthenticationResults() {
        // Arrange

        let validToken = "abCDfgHIjk0987654321"
        let expectation = expectation(description: "Unauthenticatied success expectation")

        // Act

        sut?.unauthenticate(token: validToken) { result in
            switch result {
            case .success(let success):
                print("Unauthentication success: \(success)")

                // Assert
                XCTAssertTrue(success, "Authentication should be success but it was \(success)")
                expectation.fulfill()
            case .failure(let error):
                print("Error: \(error.localizedDescription)")

                XCTAssertNil(error, "error should be nil but it was \(error.localizedDescription)")
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 4.0)
    }
*/
}
